
"""
Definition of models.
"""
from django.db import models
import datetime
import string
import time









#TEMPLATES    
class Morada(models.Model):
    
    conta = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True, blank=True)
    
    nome = models.CharField(max_length=190)
    endereco = models.CharField(max_length=190)
    localidade =  models.CharField(max_length=120)
    codigo_postal = models.CharField(max_length=10)
    pais = models.CharField(max_length=120)
    telemovel = models.IntegerField(default=0000000)
    comentarios_instrucoes = models.CharField(max_length=72,null=True,blank=True)
    
    def __str__(self):
        texto = self.nome+"\n"+self.endereco+"\n"+self.codigo_postal+"\n"+self.localidade+"\n"+self.pais+"\n"+str(self.telemovel)+"\n"+self.comentarios_instrucoes
        return texto
    
class Encomenda(models.Model):
    
    comprador = models.ForeignKey('auth.User', on_delete=models.CASCADE, null=True,blank=True)
    morada = models.ForeignKey('app.Morada', on_delete=models.CASCADE)
    
    precoProdutos = models.DecimalField(max_digits=6,decimal_places=2,default=0.00)
    precoEnvio = models.DecimalField(max_digits=6,decimal_places=2,default=0.00)
    precoTotal = models.DecimalField(max_digits=6,decimal_places=2)
    quantidadeProdutos = models.IntegerField()
    
    dataHora = models.DateTimeField(default=datetime.datetime.now)
    
    ESTADOCHOICES = (
        ('-1', "Não efectuada"),
        ('0', 'Cancelado'),
        ('1', 'A Aguardar Confirmacão de Pagamento'),
        ('2', 'Registado'),
        ('3', 'Enviado'),
        ('4', 'Em trânsito'),
        ('5', 'Entregue'),)
    
    estado = models.CharField(
        max_length=2,
        choices=ESTADOCHOICES,
        default="-1"
    )
    
    PAGAMENTOCHOICES=(
        ('Cartao de Credito','Cartao de Credito'),
        ('Paypal','Paypal'),
        ('Transferência Bancária','Transferência Bancária'),
        ('Multibanco','Multibanco'),)
    
    modoPagamento = models.CharField(
        max_length=30,
        choices=PAGAMENTOCHOICES,
        null=True
    )
    
    pagamentoEfectuado = models.BooleanField(default=False)
    
    dataExpiracaoPagamento = models.DateTimeField(null=True)
    
    codigo = models.CharField(max_length=50)
    
    email = models.EmailField(null=True,blank=True)
    visitante = models.BooleanField(default=False)
    
    def __str__(self):
        return self.get_estado_display() 
    
    def data_str(self):
        return self.dataHora.strftime("%d/%m/%Y %H:%M")


    