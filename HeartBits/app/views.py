'''
Created on 13/08/2017

@author: Miguel
'''
# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpRequest
from django.http import HttpResponse
from django.template import RequestContext
from datetime import datetime
from django.contrib.auth import authenticate
from django.contrib import auth
from app.forms import *
from app.models import *
from django.contrib.auth.models import User
from django.http.response import Http404
import json
from django.contrib.auth import logout as django_logout
from django.utils import timezone
from django.conf import settings
from django.contrib.auth import login as auth_login
from django.core.mail import EmailMessage

def home(request):
    assert isinstance(request, HttpRequest)
    context=get_context(request)
    
    return render(request,'app/home.html',context)


def encomendasTEMPLATE(request):
    assert isinstance(request, HttpRequest)
    context=get_context(request)
    
    if not request.user.is_authenticated:
        return redirect('login')
    
    user = User.objects.get(pk=request.user.id)
    encomendas = Encomenda.objects.filter(comprador=user).order_by('-dataHora').exclude(estado="-1")
    
    datas=[]
    for e in encomendas:
        datas.append(e.dataHora.strftime("%d/%m/%Y %H:%M"))
    
    context["datas"]=datas
    context["encomendas"]=encomendas
    
    return render(request,'app/encomendas.html', context);


    
def login(request):
        
    assert isinstance(request, HttpRequest)
    context=get_context(request)
    
    if request.method == 'POST':
        autenthicationForm = AuthenticationForm(request.POST)
        
        if autenthicationForm.is_valid():
            email = autenthicationForm.cleaned_data['email']
            password = autenthicationForm.cleaned_data['password']
    
            user = authenticate(username=email, password=password)
            
            if user is not None:
                auth.login(request, user)
                return redirect('home')
        
               
        context["failedLogin"]=True
        return render(request,'app/login.html', context);

    elif request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('home')
        
        return render(request,'app/login.html', context);

def registar(request):
    assert isinstance(request, HttpRequest)
    context=get_context(request)
    
    if request.method == 'POST':
        registrationForm = RegistrationForm(request.POST)

        if registrationForm.is_valid():

            email = registrationForm.cleaned_data['email']
            password = registrationForm.cleaned_data['password']
            confirm_password = registrationForm.cleaned_data['confirm_password']
            
            if User.objects.filter(email=email).exists():
                context["failedEmail"]=True
                return render(request,'app/registar.html', context);
            
            if password!=confirm_password:
                context["failedPassword"]=True
                return render(request,'app/registar.html', context);

            
            User.objects.create_user(email, email, password)
            
            return redirect('login')
        
        else:
            context["failedData"]=True
            return render(request,'app/registar.html', context);
    
    elif request.method == 'GET':
        if request.user.is_authenticated:
            return redirect('home')
        
        return render(request,'app/registar.html', context);


def logout(request):
    assert isinstance(request, HttpRequest)
    
    response = redirect("home")
    django_logout(request)
     
    return response

def test(request):
    assert isinstance(request, HttpRequest)
    context=get_context(request)
    
    return render(request,'app/test.html', context);


def get_context(request):
    context={}
    context["login"]=request.user.is_authenticated
  
    return context
        

   
