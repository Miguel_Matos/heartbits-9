"""
Definition of forms.
"""

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.forms import ModelForm, TextInput
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.template.defaultfilters import default



class AuthenticationForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()
    
    
class RegistrationForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()
    confirm_password = forms.CharField()
    